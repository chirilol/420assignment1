from flask import Flask, request, render_template
import random
app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post(): #executes when button pressed
    try:
        dividend = int( request.form['dividend'])
        divisor = int(request.form['divisor'])
    except ValueError:
        return render_template('exercise1.html', variable="Invalid Input : Enter a Number!")
    result = dividend/divisor
    temp = ""
    if dividend%2==0:
        temp = "The dividend is even"
        if dividend%4==0:
            temp = "The dividend is a multiple of 4"
    elif dividend%3==0:
        temp = "The dividend is odd"
    
    if (result)%2==0:
        temp+=" ; The dividend divides evenly by the divisor"
    output = "Result: "+str(result)+" ; "+temp
    return render_template('exercise1.html', variable=output)

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    if name == "":
        name="John Doe"
    try:
        grade = int(request.form['grade'])
        if grade>100 or grade<0:
            raise ValueError
    except ValueError:
        return render_template('exercise2.html', var="Invalid Input Type : A Number (0-100) Please")
    result=""
    if grade>=95:
        result = "A"
    elif 95>grade>=80:
        result = "B"
    elif 79>=grade>=70:
        result = "C"
    elif 69>=grade>=60:
        result = "D"
    else:
        result = "F"
    return render_template('exercise2.html', var=name+" got a grade of: "+result)

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    try:
        value = int(request.form['value'])
        if value < 0:
            raise ValueError
    except ValueError:
        return render_template('exercise3.html', var="Invalid Input : Try Again!")
    answer=value**(1/2)
    return render_template('exercise3.html', var=answer)

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    try:
        number = int(request.form['number'])
        if number>100 or number<0:
            raise ValueError
    except ValueError:
        return render_template('exercise4.html', var="Invalid Input : Enter a number between 0-100")
    count=0
    rn = 0
    while rn != number:
        rn = random.randint(0,100)
        count+=1
    return render_template('exercise4.html', var="It took "+str(count)+" random tries to match the inputted number.")

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    try:
        number = int(request.form['number'])
    except ValueError:
        return render_template('exercise5.html', var="Invalid Input : Try Again!")
    if number <= 1:
        return render_template('exercise5.html', var=str(number)+": error... limit is number>1")
    one=0
    two=1
    sum=0
    string = str(number)+": "+str(one)+", "+str(two)+", "
    while sum<number:
        sum=one+two
        one=two
        two=sum
        if sum>number:
            break
        string += str(sum)+", "
    return render_template('exercise5.html', var=string)